import tweepy, os
from tweepy import Stream
from tweepy import OAuthHandler
from tweepy import API
from tweepy.streaming import StreamListener

consumer_key = ""
consumer_secret = ""
access_token = ""
access_token_secret = ""

command = ["command:",]
attr = ["run", "tweet", "tweetAuto"]
arg = [""]
commands = ["say: Hello World",
            "say: Cookies",
            "command: launchNotepad"]

class StdOutListener( StreamListener ):

    def __init__( self ):
        self.tweetCount = 0

    def on_connect( self ):
        print("Connection established!!")

    def on_disconnect( self, notice ):
        print("Connection lost!! : ", notice)

    def on_data( self, status ):
        print("Entered on_data()")
        print(status, flush = True)
        contained = [x for x in commands if x in status]
        print(contained)
        if contained == ["say: Hello World"]:
            print("Hello World! :D")
        elif contained == ["say: Cookies"]:
            print("COOKIEZ!")
        elif contained == ["command: launchNotepad"]:
            #filepath = "C:\\WINDOWS\\system32\\notepad.exe"
            os.system("notepad.exe")
            print("Launching Notepad...")
        elif contained == ["command: launchApps"]:
            cmdcode = """


"""

        else:
            print("No commands detected!")
        return True
        

    def on_direct_message( self, status ):
        print("Entered on_direct_message()")
        try:
            print(status, flush = True)
            return True
        except BaseException as e:
            print("Failed on_direct_message()", str(e))

    def on_error( self, status ):
        print(status)

def main():

    try:
        auth = OAuthHandler(consumer_key, consumer_secret)
        auth.secure = True
        auth.set_access_token(access_token, access_token_secret)

        api = API(auth)

        # If the authentication was successful, you should
        # see the name of the account print out
        print(api.me().name)

        stream = Stream(auth, StdOutListener())

        stream.userstream()

    except BaseException as e:
        print("Error in main()", e)

if __name__ == '__main__':
    main()

def updStatus(mention):
    if mention != "":
        mention = "@" + mention
        status = (mention, input("Tweet: "))
        status = " ".join(status)

    else:
        status = input("Tweet: ")
        
            
        
    if len(status) > 140:
        print(len(status))
        print("Tweet is too long!")
        updStatus(input("Mention a person: "))

    elif status == "":
        print("Empty Tweet.")
        pass
        
    else:
        print(status)
        print(len(status))
        api.update_status(status)




updStatus(input("Mention a person: "))
